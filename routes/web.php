<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');

Route::get('/registration', function () {
    return view('frontend.signup');
});

Route::post('/confirmation','HomeController@save_register');
Route::post('/success','HomeController@thankpost');

Route::get('check_promo','HomeController@chekpromo'); //ajax

Route::get('/success', function () {
    return view('frontend.success');
    
});

Route::get('/registration-success', function () {
   
    return view('frontend.successforeign');
});

Route::get('/failure', function () {
    return view('frontend.failure');
});


Route::get('/delegates/login',function () {
    return view('frontend.login');
});

Route::post('/delegate/login','EventController@login');

Route::get('/user/logout', 'EventController@logout');

//Route::get('/generateabcpromo','HomeController@generatepromo');


//refresh_poll

Route::get('/get_city','HomeController@get_city');
Route::get('/check_userdetails','Auth\RegisterController@check_userdetails');


// Register
Route::get('/check-user','Auth\RegisterController@check_user'); // Ajax
Route::get('/check-mail_con','Auth\RegisterController@check_user_mem');
Route::get('/verify_otp','Auth\RegisterController@verify_otp'); //ajax
Route::get('/login-form','UiController@login'); // Ajax



Auth::routes();
Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::get('/register/admin', 'Auth\RegisterController@showAdminRegisterForm');

Route::post('/admin/login', 'Auth\LoginController@adminLogin');
Route::post('/register/admin', 'Auth\RegisterController@createAdmin');



// ADMIN
Route::get('/admin', 'DashboardController@index')->name('Admin dashboard');
Route::get('/admin/paid/registration', 'AdminController@index');
Route::get('/admin/unpaid/registration', 'AdminController@indexunpaid');
Route::get('/admin/promocodes', 'AdminController@promocodes');
Route::get('/admin/member/delete/{id}', 'AdminController@deletem');

Route::get('/admin/activity/view/{id}', 'AdminController@view_details');

Route::get('/admin/visitors/details', 'AdminController@visitors');
Route::get('/admin/logout', 'AdminController@logout');
Route::get('/admin/session/details','AdminController@session_list');
Route::get('/admin/session/status/{sts}/{ids}','AdminController@activate_session');

// Excel Export
Route::get('/admin/unpaid/registration/export/excel','ExportExcelController@total_excel');
Route::get('/admin/paid/registration/export/excel','ExportExcelController@today_excel');
Route::get('/admin/total/activity/export/excel','ExportExcelController@actvity_excel');

Route::get('/question/export/excel','ExportExcelController@question_excel');
Route::get('/feedback/export/excel/{fid}','ExportExcelController@feedback_excel');
// Route::get('/home1', 'CustomHomeController@index');
// Route::get('/logout/user', 'CustomHomeController@logoutUser');
// Route::post('/register/user', 'CustomSignupController@addUser');
// Route::post('/login/user', 'CustomLoginController@loginUser');
