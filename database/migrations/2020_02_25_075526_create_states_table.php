<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		if (!Schema::hasTable('states')) {
			Schema::create('states', function (Blueprint $table) {
            $table->bigIncrements('state_id');
			$table->char('name',150);
			$table->integer('country_id')->usigned();;
			$table->foreign("country_id")->references('country_id')->on('countries');
         });
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('states');
    }
}
