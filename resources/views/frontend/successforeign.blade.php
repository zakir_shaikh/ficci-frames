@include('frontend.layout.header')
	<br />
	<style>p{margin-bottom:8px !important;} </style>
	<section id="success">
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col-lg-10">
					<div class="card shade text-center">
						<img src="/img/success.jpg" class="img-fluid mx-auto d-block" />
						
						<h1 style="font-size:30px; margin:10px 0;"> Dear @if(null !==session()->get('dname'))
						 {{ucfirst(session()->get('dname'))}}, @else Name, @endif <br />

						 Thanks for registering!! </h1>

						<p> To complete the process, kindly use the details given below and make the payment: </p>

						<strong>Currency:</b> USD</strong>
						<p><b>Account Name:</b> Federation of Indian Chambers of Commerce and Industry</p>
						<p><b>SB Account No. :</b> 013694600000041</p>
						<p><b>Bank Name :</b> Yes Bank Ltd</p>
						<p><b>Bank Address :</b> 56 Janpath, Alps Builiding, Connaught Place , New Delhi 110001</p>

						<p><b>Swift Code - Yes Bank India : YESBINBBXXX </b></p>
						<p><b>IFSC CODE : </b> YESB0000136 </p> 

						<strong>Correspondent Bank Details</strong> 
						<p><b>Bank Name :</b> BANK Of NEW YORK , NEW YORK</p>
						<p><b>Swift Code :</b> IRVTUS3N</p>
						<p><b>Account number :</b> 890-057-5263</p>

						<strong> Registrations :</strong> <a href="mailto:frames.registration@ficci.com"> frames.registration@ficci.com </a> <br />
						<strong> Programme :</strong>  <a href="mailto:amit.tyagi@ficci.com"> amit.tyagi@ficci.com </a> <br />
						<strong> Sponsorship's/E-stalls/Advertisement :</strong>  <a href="mailto:samir.kumar@ficci.com"> samir.kumar@ficci.com </a> <br />

						<p> Team FICCI FRAMES </p>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section id="partners" class="section-with-bg">
		<div class="container-fluid">
			<div class="section-header">
				<h2>Partners</h2>
			</div>
		<center>	<div class="col-lg-3 col-md-4 col-xs-6" style="padding-bottom:10px;">
					<a href="https://www.startv.com/" target="_blank" class="supporter-logo" style="border-bottom:none;border-right:none;"> 
						<p> <strong> Convention partner </strong> </p> <img src="{{asset('img/sponsor/star.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div> </center>
			<div class="row no-gutters supporters-wrap clearfix">
				
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.intel.in/content/www/in/en/homepage.html" target="_blank"  class="supporter-logo"> 
						<p> <strong> Diamond partner & BAF Awards  Co-Presenter </strong></p><img src="{{asset('img/sponsor/intel.jpg')}}" class="img-fluid" alt="">   
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.netflix.com/in/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Gold partner </strong> </p> <img src="{{asset('img/sponsor/netflix.jpg')}}" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.motionpictures.org/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate partner </strong> </p>  <img src="{{asset('img/sponsor/mpa.jpg')}}" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.discovery.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate Partner </strong> </p>  <img src="{{asset('img/sponsor/discovery.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.ufomoviez.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Cine Media Partner </strong> </p> <img src="{{asset('img/sponsor/ufo.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.mediabrief.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p> <img src="{{asset('img/sponsor/media_brief.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.medianews4u.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="{{asset('img/sponsor/media_news.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				

				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.adgully.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="{{asset('img/sponsor/adgully.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>

				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.mxplayer.in/" target="_blank"  class="supporter-logo"> 
						<p> <strong> OTT Partner </strong> </p>  <img src="{{asset('img/sponsor/mx-player.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>

			</div>
		</div>
	</section>  
	
@include('frontend.layout.footer')

