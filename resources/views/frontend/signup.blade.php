@include('frontend.layout.header')

<script language="javascript">
$(document).ready(function() {
	
    $("body").on("click",".add-more",function(){ 
		
        var html = $(".after-add-more").first().clone();      
        //  $(html).find(".change").prepend("<label for=''>&nbsp;</label><br/><a class='btn btn-danger remove'>- Remove</a>");      
          $(html).find(".change").html("<label for=''>&nbsp;</label><br/><a class='btn btn-danger remove'>- Remove</a>");
		$(".after-add-more").last().after(html);
		
    });
    $("body").on("click",".remove",function(){ 
        $(this).parents(".after-add-more").remove();
    });
});
</script>


	<br /><br />
	<section class="register_form">
		<div class="container">
            <div class="row justify-content-md-center"> 
				<div class="col-12">
					<h2 class="text-center"> REGISTER </h2>
					<div class="title-border"><span></span></div>
				</div>
                <div class="col-lg-10"> 
					<div class="card"> 
						<form name="form1" method="post" action="confirmation" onSubmit="validate();"  class="contact100-form validate-form">
						@csrf

						<input type="hidden" name="RegId" value="<?=rand(100000,999999)?>">
							<div class="form-group required">
								<div class="row">
									<label for="colFormLabelSm" class="bold col-sm-3 col-form-label">Choose Category  </label>
									<div class="col-sm-9" style="padding-top:7px;">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="nationality"  value="Indian National" checked onClick="feeCalculation()">
											<label class="form-check-label" for="inlineRadio1"> Indian National </label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="nationality"  value="Foreign National" onClick="feeCalculation()">
											<label class="form-check-label" for="inlineRadio2"> Foreign National </label>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group required">
								<div class="row">
									<label for="colFormLabelSm" class="bold col-sm-3 col-form-label">Register As </label>
									<div class="col-sm-9">
										<select name="typeIndustry" class="custom-select" onchange="feeCalculation()">
											<option value="">Please Select Register Type </option>
											<option value="FICCI Corporate Member">FICCI Corporate Member</option>
											<option value="FICCI Associate Member">FICCI Associate Member</option>
											<option value="Non Member">Non Member</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group day_check text-center required">
								<div class="form-check">
									<input class="form-check-input"  onclick="feeCalculation()" id="inlineCheckbox1" type="checkbox" name="category[]" value="7th July 2020" >
									<label class="form-check-label" for="inlineCheckbox1">Day 1<br /> (7th July, 2020)</label>
								</div>
								<div class="form-check">
									<input class="form-check-input"  onclick="feeCalculation()" id="inlineCheckbox2" type="checkbox" name="category[]" value="8th July 2020" >
									<label class="form-check-label" for="inlineCheckbox2">Day 2<br /> (8th July, 2020)</label>
								</div>
								<div class="form-check">
									<input class="form-check-input"  onclick="feeCalculation()" id="inlineCheckbox3" type="checkbox" name="category[]" value="9th July 2020" >
									<label class="form-check-label" for="inlineCheckbox3">Day 3<br /> (9th July, 2020)</label>
								</div>
								<div class="form-check">
									<input class="form-check-input"  onclick="feeCalculation()" id="inlineCheckbox4" type="checkbox" name="category[]" value="10th July 2020" >
									<label class="form-check-label" for="inlineCheckbox4">Day 4<br /> (10th July, 2020)</label>
								</div>
								<div class="form-check">
									<input class="form-check-input"  onclick="feeCalculation()" id="inlineCheckbox5" type="checkbox" name="category[]" value="11th July 2020" >
									<label class="form-check-label" for="inlineCheckbox5">Day 5 <br /> (11th July, 2020)</label>
								</div>

								<div class="form-check">
									<input class="form-check-input"  onclick="feeCalculation()"  id="inlineCheckbox2" type="checkbox" name="category[]" value="7th - 11th July 2020" >
									<label class="form-check-label" for="inlineCheckbox2"> All Day's <br /> (7th - 11th July, 2020)</label>
								</div>
								<br /> <br />
								<p class="d-block" id="calFee"> </p>
							</div>
							<div class="form-group required">
								<div class="row">
									<label for="colFormLabelSm" class="bold col-sm-3 col-form-label">Membership No.</label>
									<div class="col-sm-9">
										<input name="MembershipNo" class="form-control" maxlength="255" type="text"value=""  placeholder="Membership No*" id="membership_no" required="required" />
									</div>
								</div>
							</div>
											
							<div class="form-group text-left">
								<label class="bold"> Name of delegate(s) </label>
								<div class="row after-add-more" >
									<div class="col-sm-5">                                
										<div class="form-group deletgae">
											<label class="control-label">Full Name</label>
											<input name="Name[]" class="form-control" maxlength="255" type="text" value=""  placeholder="Full Name*" id="Name" required="required" />
										</div>
									</div>
									<div class="col-sm-5">
										<div class="form-group">
											<label class="control-label">Designation</label>
											<input name="Designation[]" class="form-control" maxlength="255" type="text" value=""  placeholder="Designation*" id="Designation" required="required" />
										</div>
									</div>
									<div class="col-sm-2">
										<div class="form-group change">
											<label for="">&nbsp;</label><br/>
											<a class="btn btn-warning add-more">+ Add More</a>
										</div>
									</div>
								</div>
							</div> <!-- -->
							<div class="row">
								<div class="col-sm-6 form-group required">
									<label class="bold"> Organisation </label>
									<input name="Organisation" class="form-control" maxlength="255" type="text" value=""  placeholder="Organisation*" id="Organisation" required="required" />
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> Mailing Address </label>
									<input name="Address" class="form-control" maxlength="255" type="text" value=""  placeholder="Mailing Address*" id="Address"  required="required" />
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> Country </label>
									<select name="country" placeholder="Country" title="Country" id="country" class="custom-select" onchange="return statechange()" required="required">
										<option value="">Please Select Country </option>
										<option value="">Country...</option>
<option value="Afganistan">Afghanistan</option>
<option value="Albania">Albania</option>
<option value="Algeria">Algeria</option>
<option value="American Samoa">American Samoa</option>
<option value="Andorra">Andorra</option>
<option value="Angola">Angola</option>
<option value="Anguilla">Anguilla</option>
<option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
<option value="Argentina">Argentina</option>
<option value="Armenia">Armenia</option>
<option value="Aruba">Aruba</option>
<option value="Australia">Australia</option>
<option value="Austria">Austria</option>
<option value="Azerbaijan">Azerbaijan</option>
<option value="Bahamas">Bahamas</option>
<option value="Bahrain">Bahrain</option>
<option value="Bangladesh">Bangladesh</option>
<option value="Barbados">Barbados</option>
<option value="Belarus">Belarus</option>
<option value="Belgium">Belgium</option>
<option value="Belize">Belize</option>
<option value="Benin">Benin</option>
<option value="Bermuda">Bermuda</option>
<option value="Bhutan">Bhutan</option>
<option value="Bolivia">Bolivia</option>
<option value="Bonaire">Bonaire</option>
<option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
<option value="Botswana">Botswana</option>
<option value="Brazil">Brazil</option>
<option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
<option value="Brunei">Brunei</option>
<option value="Bulgaria">Bulgaria</option>
<option value="Burkina Faso">Burkina Faso</option>
<option value="Burundi">Burundi</option>
<option value="Cambodia">Cambodia</option>
<option value="Cameroon">Cameroon</option>
<option value="Canada">Canada</option>
<option value="Canary Islands">Canary Islands</option>
<option value="Cape Verde">Cape Verde</option>
<option value="Cayman Islands">Cayman Islands</option>
<option value="Central African Republic">Central African Republic</option>
<option value="Chad">Chad</option>
<option value="Channel Islands">Channel Islands</option>
<option value="Chile">Chile</option>
<option value="China">China</option>
<option value="Christmas Island">Christmas Island</option>
<option value="Cocos Island">Cocos Island</option>
<option value="Colombia">Colombia</option>
<option value="Comoros">Comoros</option>
<option value="Congo">Congo</option>
<option value="Cook Islands">Cook Islands</option>
<option value="Costa Rica">Costa Rica</option>
<option value="Cote DIvoire">Cote D'Ivoire</option>
<option value="Croatia">Croatia</option>
<option value="Cuba">Cuba</option>
<option value="Curaco">Curacao</option>
<option value="Cyprus">Cyprus</option>
<option value="Czech Republic">Czech Republic</option>
<option value="Denmark">Denmark</option>
<option value="Djibouti">Djibouti</option>
<option value="Dominica">Dominica</option>
<option value="Dominican Republic">Dominican Republic</option>
<option value="East Timor">East Timor</option>
<option value="Ecuador">Ecuador</option>
<option value="Egypt">Egypt</option>
<option value="El Salvador">El Salvador</option>
<option value="Equatorial Guinea">Equatorial Guinea</option>
<option value="Eritrea">Eritrea</option>
<option value="Estonia">Estonia</option>
<option value="Ethiopia">Ethiopia</option>
<option value="Falkland Islands">Falkland Islands</option>
<option value="Faroe Islands">Faroe Islands</option>
<option value="Fiji">Fiji</option>
<option value="Finland">Finland</option>
<option value="France">France</option>
<option value="French Guiana">French Guiana</option>
<option value="French Polynesia">French Polynesia</option>
<option value="French Southern Ter">French Southern Ter</option>
<option value="Gabon">Gabon</option>
<option value="Gambia">Gambia</option>
<option value="Georgia">Georgia</option>
<option value="Germany">Germany</option>
<option value="Ghana">Ghana</option>
<option value="Gibraltar">Gibraltar</option>
<option value="Great Britain">Great Britain</option>
<option value="Greece">Greece</option>
<option value="Greenland">Greenland</option>
<option value="Grenada">Grenada</option>
<option value="Guadeloupe">Guadeloupe</option>
<option value="Guam">Guam</option>
<option value="Guatemala">Guatemala</option>
<option value="Guinea">Guinea</option>
<option value="Guyana">Guyana</option>
<option value="Haiti">Haiti</option>
<option value="Hawaii">Hawaii</option>
<option value="Honduras">Honduras</option>
<option value="Hong Kong">Hong Kong</option>
<option value="Hungary">Hungary</option>
<option value="Iceland">Iceland</option>
<option value="India">India</option>
<option value="Indonesia">Indonesia</option>
<option value="Iran">Iran</option>
<option value="Iraq">Iraq</option>
<option value="Ireland">Ireland</option>
<option value="Isle of Man">Isle of Man</option>
<option value="Israel">Israel</option>
<option value="Italy">Italy</option>
<option value="Jamaica">Jamaica</option>
<option value="Japan">Japan</option>
<option value="Jordan">Jordan</option>
<option value="Kazakhstan">Kazakhstan</option>
<option value="Kenya">Kenya</option>
<option value="Kiribati">Kiribati</option>
<option value="Korea North">Korea North</option>
<option value="Korea Sout">Korea South</option>
<option value="Kuwait">Kuwait</option>
<option value="Kyrgyzstan">Kyrgyzstan</option>
<option value="Laos">Laos</option>
<option value="Latvia">Latvia</option>
<option value="Lebanon">Lebanon</option>
<option value="Lesotho">Lesotho</option>
<option value="Liberia">Liberia</option>
<option value="Libya">Libya</option>
<option value="Liechtenstein">Liechtenstein</option>
<option value="Lithuania">Lithuania</option>
<option value="Luxembourg">Luxembourg</option>
<option value="Macau">Macau</option>
<option value="Macedonia">Macedonia</option>
<option value="Madagascar">Madagascar</option>
<option value="Malaysia">Malaysia</option>
<option value="Malawi">Malawi</option>
<option value="Maldives">Maldives</option>
<option value="Mali">Mali</option>
<option value="Malta">Malta</option>
<option value="Marshall Islands">Marshall Islands</option>
<option value="Martinique">Martinique</option>
<option value="Mauritania">Mauritania</option>
<option value="Mauritius">Mauritius</option>
<option value="Mayotte">Mayotte</option>
<option value="Mexico">Mexico</option>
<option value="Midway Islands">Midway Islands</option>
<option value="Moldova">Moldova</option>
<option value="Monaco">Monaco</option>
<option value="Mongolia">Mongolia</option>
<option value="Montserrat">Montserrat</option>
<option value="Morocco">Morocco</option>
<option value="Mozambique">Mozambique</option>
<option value="Myanmar">Myanmar</option>
<option value="Nambia">Nambia</option>
<option value="Nauru">Nauru</option>
<option value="Nepal">Nepal</option>
<option value="Netherland Antilles">Netherland Antilles</option>
<option value="Netherlands">Netherlands (Holland, Europe)</option>
<option value="Nevis">Nevis</option>
<option value="New Caledonia">New Caledonia</option>
<option value="New Zealand">New Zealand</option>
<option value="Nicaragua">Nicaragua</option>
<option value="Niger">Niger</option>
<option value="Nigeria">Nigeria</option>
<option value="Niue">Niue</option>
<option value="Norfolk Island">Norfolk Island</option>
<option value="Norway">Norway</option>
<option value="Oman">Oman</option>
<option value="Pakistan">Pakistan</option>
<option value="Palau Island">Palau Island</option>
<option value="Palestine">Palestine</option>
<option value="Panama">Panama</option>
<option value="Papua New Guinea">Papua New Guinea</option>
<option value="Paraguay">Paraguay</option>
<option value="Peru">Peru</option>
<option value="Phillipines">Philippines</option>
<option value="Pitcairn Island">Pitcairn Island</option>
<option value="Poland">Poland</option>
<option value="Portugal">Portugal</option>
<option value="Puerto Rico">Puerto Rico</option>
<option value="Qatar">Qatar</option>
<option value="Republic of Montenegro">Republic of Montenegro</option>
<option value="Republic of Serbia">Republic of Serbia</option>
<option value="Reunion">Reunion</option>
<option value="Romania">Romania</option>
<option value="Russia">Russia</option>
<option value="Rwanda">Rwanda</option>
<option value="St Barthelemy">St Barthelemy</option>
<option value="St Eustatius">St Eustatius</option>
<option value="St Helena">St Helena</option>
<option value="St Kitts-Nevis">St Kitts-Nevis</option>
<option value="St Lucia">St Lucia</option>
<option value="St Maarten">St Maarten</option>
<option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
<option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
<option value="Saipan">Saipan</option>
<option value="Samoa">Samoa</option>
<option value="Samoa American">Samoa American</option>
<option value="San Marino">San Marino</option>
<option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
<option value="Saudi Arabia">Saudi Arabia</option>
<option value="Senegal">Senegal</option>
<option value="Serbia">Serbia</option>
<option value="Seychelles">Seychelles</option>
<option value="Sierra Leone">Sierra Leone</option>
<option value="Singapore">Singapore</option>
<option value="Slovakia">Slovakia</option>
<option value="Slovenia">Slovenia</option>
<option value="Solomon Islands">Solomon Islands</option>
<option value="Somalia">Somalia</option>
<option value="South Africa">South Africa</option>
<option value="Spain">Spain</option>
<option value="Sri Lanka">Sri Lanka</option>
<option value="Sudan">Sudan</option>
<option value="Suriname">Suriname</option>
<option value="Swaziland">Swaziland</option>
<option value="Sweden">Sweden</option>
<option value="Switzerland">Switzerland</option>
<option value="Syria">Syria</option>
<option value="Tahiti">Tahiti</option>
<option value="Taiwan">Taiwan</option>
<option value="Tajikistan">Tajikistan</option>
<option value="Tanzania">Tanzania</option>
<option value="Thailand">Thailand</option>
<option value="Togo">Togo</option>
<option value="Tokelau">Tokelau</option>
<option value="Tonga">Tonga</option>
<option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
<option value="Tunisia">Tunisia</option>
<option value="Turkey">Turkey</option>
<option value="Turkmenistan">Turkmenistan</option>
<option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
<option value="Tuvalu">Tuvalu</option>
<option value="Uganda">Uganda</option>
<option value="Ukraine">Ukraine</option>
<option value="United Arab Erimates">United Arab Emirates</option>
<option value="United Kingdom">United Kingdom</option>
<option value="United States of America">United States of America</option>
<option value="Uraguay">Uruguay</option>
<option value="Uzbekistan">Uzbekistan</option>
<option value="Vanuatu">Vanuatu</option>
<option value="Vatican City State">Vatican City State</option>
<option value="Venezuela">Venezuela</option>
<option value="Vietnam">Vietnam</option>
<option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
<option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
<option value="Wake Island">Wake Island</option>
<option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
<option value="Yemen">Yemen</option>
<option value="Zaire">Zaire</option>
<option value="Zambia">Zambia</option>
<option value="Zimbabwe">Zimbabwe</option>
									</select>
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> State (For Domestic Participants Only) </label>
									<select name="State" placeholder="State" class="custom-select" title="State" id="State"  class="form-control">
										<option value="">Please Select State </option>
	  <option value="Andaman and Nicobar Islands:AN"> Andaman and Nicobar Islands</option>
	<option value="Andhra Pradesh:AD">Andhra Pradesh</option>
<option value="Arunachal Pradesh:AR">Arunachal Pradesh</option>
<option value="Assam:AS">Assam</option>
<option value="Bihar:BR">Bihar</option>
<option value="Chandigarh:CH">Chandigarh</option>
<option value="Chhattisgarh:CG">Chhattisgarh</option>
<option value="Dadra and Nagar Haveli:DN">Dadra and Nagar Haveli</option>
<option value="Daman and Diu:DD">Daman and Diu</option>
<option value="Delhi:DL">Delhi</option>
<option value="Goa:GA">Goa</option>
<option value="Gujarat:GJ">Gujarat</option>
<option value="Haryana:HR">Haryana</option>
<option value="Himachal Pradesh:HP">Himachal Pradesh</option>
<option value="Jammu and Kashmir:JK">Jammu and Kashmir</option>
<option value="Jharkhand:JH">Jharkhand</option>
<option value="Karnataka:KA">Karnataka</option>
<option value="Kerala:KL">Kerala</option>
<option value="Lakshadweep Islands:LD">Lakshadweep Islands</option>
<option value="Madhya Pradesh:MP">Madhya Pradesh</option>
<option value="Maharashtra:MH">Maharashtra</option>
<option value="Manipur:MN">Manipur</option>
<option value="Meghalaya:ML">Meghalaya</option>
<option value="Mizoram:MZ">Mizoram</option>
<option value="Nagaland:NL">Nagaland</option>
<option value="Odisha:OD">Odisha</option>
<option value="Pondicherry:PY">Pondicherry</option>
<option value="Punjab:PB">Punjab</option>
<option value="Rajasthan:RJ">Rajasthan</option>
<option value="Sikkim:SK">Sikkim</option>
<option value="Tamil Nadu:TN">Tamil Nadu</option>
<option value="Telangana:TL">Telangana</option>
<option value="Tripura:TR">Tripura</option>
<option value="Uttar Pradesh:UP">Uttar Pradesh</option>
<option value="Uttarakhand:UK">Uttarakhand</option>
<option value="West Bengal:WB">West Bengal</option>
</select>
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> City </label>
									<input name="City" class="form-control" maxlength="255" type="text" value=""  placeholder="City*" id="city" required="required" />
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> Pin/Zip/Postal Code</label>
									<input name="ZipCode" class="form-control" maxlength="255" type="number" value=""  placeholder="Pin/Zip/Postal Code*" id="ZipCode" required="required" />
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> Do you have a registered GSTN </label>
									<select id="gstny" name="gstny"  class="custom-select"  onchange="return statechange1()" required="required">
										<option value=""> Please Select  </option>
										<option value="YES"> YES </option>
										<option value="NO"> NO </option>
									</select>
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> Please provide GST No. (If Applicable) </label>
									<input type="text"  class="form-control inp"  name="GSTNumber" id="GSTNumber"  class="inp form-control" disabled>
									<small class="red"> Provided GST No. should be issued at given mailing address for invoicing purpose. </small>
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> Telephone </label>
									<input name="Telephone" class="form-control" maxlength="255" type="text" value=""  placeholder="Telephone" id="Telephone" />
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> Mobile </label>
									<input name="Mobile" class="form-control" maxlength="255" type="number" value=""  placeholder="Mobile*" id="Mobile" required="required" />
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> Fax </label>
									<input name="Fax" class="form-control" maxlength="255" type="text" value=""  placeholder="Fax" id="fax"  />
								</div>
								<div class="col-sm-6 form-group required">
									<label class="bold"> E-mail </label>
									<input name="OfficialEmail" class="form-control" maxlength="255" type="email" value=""  placeholder="E-mail*" id="Email" required="required" />
								</div>
								<div class="col-sm-12 form-group required">
									<label class="bold"> Website </label>
									<input name="Website" class="form-control" maxlength="255" type="text" value=""  placeholder="Website" id="Website"  />
								</div>
							</div> <!-- Row -->
							<div class="form-group business_category required">
								<label class="bold"> Business Category </label>
								<div class="form-inline">
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="Animation" value="Animation" >
										<label class="form-check-label" for="Animation"> Animation </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="Gaming" value="Gaming">
										<label class="form-check-label" for="Gaming"> Gaming </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="VFX" value="VFX"> 
										<label class="form-check-label" for="VFX"> VFX </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="TV" value="TV">
										<label class="form-check-label" for="TV"> TV </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="Radio" value="Radio"> 
										<label class="form-check-label" for="Radio"> Radio</label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="sales_agent" value="Sales_Agent">
										<label class="form-check-label" for="sales_agent"> Sales Agent </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="Buyers" value="Buyers">
										<label class="form-check-label" for="Buyers"> Buyers </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="Distribution" value="Distribution"> 
										<label class="form-check-label" for="Distribution"> Distribution </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="film_commission" value="Film_Commission">
										<label class="form-check-label" for="film_commission"> Film Commission </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="film_festival" value="Film_Festival"> 
										<label class="form-check-label" for="film_festival"> Film Festival </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="film_fund" value="Film_fund">
										<label class="form-check-label" for="film_fund"> Film Fund </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="financial_institution" value="Financial_Institution">
										<label class="form-check-label" for="financial_institution"> Financial Institution </label>
									</div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="production_company" value="Production_Company"> 
										<label class="form-check-label" for="production_company"> Production Company </label>
									</div>
									<div class="clear w-100"></div>
									<div class="form-check">
										<input name="b_int[]"  class="form-check-input" type="checkbox" id="other" value="Others">
										<label class="form-check-label" for="other"> Any other interest ( please specify) </label>
									</div>
								</div>
								<div class="d-block w-100">
									<input name="b_int[]" class="form-control" maxlength="255" type="text" value=""  placeholder="Any other interest ( please specify)" id="aint"  />
								</div>
							</div> <!-- -->
							<div class="form-group required">
								<div class="row">
									<label for="colFormLabelSm" class="bold col-sm-3 col-form-label">Company Profile (100 words only)</label>
									<div class="col-sm-9">
										<textarea name="personal_profile" placeholder="Company Profile (100 words only)...."  class="form-control" id="message" class="required" role="textbox" required="required"></textarea>
									</div>
								</div>
							</div>
							<div class="form-group required">
								<div class="row">
									<label for="colFormLabelSm" class="bold col-sm-3 col-form-label">Business Interest (100 words only) </label>
									<div class="col-sm-9">
										<textarea name="current_business_interest" placeholder="Business Interest (100 words only)..."  class="form-control" id="message" class="required" role="textbox" required="required"></textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
							<div class="row">
								<div class="col-sm-6 form-group required">
									<label class="bold"> Promo Code </label>
									<input name="Promocode" class="form-control" maxlength="255" type="text" value=""  placeholder="Promocode is applicable for one delegate only." id="Promocode" onblur="check_promo()"/>
									<b id="promoalert"> </b>
								</div>
							</div>
							</div>
							<div class="form-group">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="" id="agree" name="agree" required onclick="check_promo()">
									<label class="form-check-label" for="agree"> I Agree (Note: There will be no auto chargeback under any circumstances)</label>
								</div>
							</div>
							<div class="submit">
								
								<input type="hidden" name="dynamicText" value="1" />
								
							    <input class="form-control" type="hidden" name="Amount" value="" required/>
                                <input class="form-control" type="hidden" name="quantity" value="1" />
                                <input class="form-control" type="hidden" name="pageurl" value="{{ url('/success') }}" />
                                <input type="submit" name="Submit" value="Submit Form"  id="savedata" class="btn w-100 primary-btn"  onClick="return validate()"> 
							</div>
						</form>					
					</div> <!-- -->
                </div> <!-- -->
            </div> <!-- Row -->
        </div> <!-- Container -->
	</section>
	
	
	
	<section id="partners" class="section-with-bg">
		<div class="container-fluid">
			<div class="section-header">
				<h2>Partners</h2>
			</div>
			<div class="row no-gutters supporters-wrap clearfix">
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/star.jpg" class="img-fluid" alt=""> <p> Star - Convention partner </p> </div>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/italy.jpg" class="img-fluid" alt=""> <p> Italy- Partner Country </p> </div>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/intel.jpg" class="img-fluid" alt=""> <p> Intel- Diamond partner & BAF Awards  Co-Presenter</p>  </div>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/netflix.jpg" class="img-fluid" alt=""> <p> NETFLIX- Gold partner </p> </div>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<div class="supporter-logo"> <img src="img/sponsor/mpa.jpg" class="img-fluid" alt=""> <p> Motion Picture Association Pvt ltd.(MPA) - Associate partner </p> </div>
				</div>
			</div>
		</div>
	</section>
	
	
@include('frontend.layout.footer')

<script language="javascript">
function feeCalculation()
{
var TotalFee=0;
var cur="";
if (document.form1.typeIndustry.value == "Non Member") {  
	  document.form1.MembershipNo.disabled="disabled";
	  document.form1.MembershipNo.placeholder="Membership No";
	} else {
		document.form1.MembershipNo.disabled=false;
		document.form1.MembershipNo.placeholder="Membership No*";
		
	}
if (document.form1.nationality.value == "Indian National"){
	//		document.form1["category[]"][0].checked=false;
	//		document.form1["category[]"][1].checked=false;
	//		document.form1["category[]"][2].checked=false;
	//		document.form1["category[]"][3].checked=false;
	//		document.form1["category[]"][4].checked=false;
	//		document.form1["category[]"][5].checked=false;


      if (document.form1.typeIndustry.value == "") {
            alert("Please select type of participant")
			document.form1["category[]"][0].checked=false;
			document.form1["category[]"][1].checked=false;
			document.form1["category[]"][2].checked=false;
			document.form1["category[]"][3].checked=false;
			document.form1["category[]"][4].checked=false;
			document.form1["category[]"][5].checked=false;
            document.form1.typeIndustry.focus()
            return false;
        }
		 if (document.form1["category[]"][0].checked == false && document.form1["category[]"][1].checked == false && document.form1["category[]"][3].checked==false && document.form1["category[]"][4].checked==false & document.form1["category[]"][5].checked==false) {
            document.form1["category[]"][0].focus()
            return false;
        }

		if(document.form1["category[]"][5].checked==true){
			document.form1["category[]"][0].checked=true;
			document.form1["category[]"][1].checked=true;
			document.form1["category[]"][2].checked=true;
			document.form1["category[]"][3].checked=true;
			document.form1["category[]"][4].checked=true;

			if (document.form1.typeIndustry.value == "FICCI Corporate Member") {
			TotalFee=2625;
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "FICCI Associate Member") {
			TotalFee=3150;	
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "Non Member") {

			TotalFee=3500;	
			cur="INR";
			}
		}


		document.form1.MembershipNo.disabled=false;
      if(document.form1["category[]"][0].checked){
        if (document.form1.typeIndustry.value == "FICCI Corporate Member") {
			TotalFee=TotalFee+750;
			cur="INR";
			
			}
		if (document.form1.typeIndustry.value == "FICCI Associate Member") {
			TotalFee=TotalFee+900;	
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "Non Member") {
			TotalFee=TotalFee+1000;	
			cur="INR";
			}
      }
	  
	  if(document.form1["category[]"][1].checked){
        if (document.form1.typeIndustry.value == "FICCI Corporate Member") {
			TotalFee=TotalFee+750;
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "FICCI Associate Member") {
			TotalFee=TotalFee+900;	
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "Non Member") {
			TotalFee=TotalFee+1000;	
			cur="INR";
			}
			
      }
	  
	  if(document.form1["category[]"][2].checked){
        if (document.form1.typeIndustry.value == "FICCI Corporate Member") {
			TotalFee=TotalFee+750;
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "FICCI Associate Member") {
			TotalFee=TotalFee+900;	
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "Non Member") {
			TotalFee=TotalFee+1000;	
			cur="INR";
			}
			
      }
	  
	  if(document.form1["category[]"][3].checked){
       if (document.form1.typeIndustry.value == "FICCI Corporate Member") {
			TotalFee=TotalFee+750;
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "FICCI Associate Member") {
			TotalFee=TotalFee+900;	
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "Non Member") {
			TotalFee=TotalFee+1000;	
			cur="INR";
			}
			
      }
	  
	  if(document.form1["category[]"][4].checked){
       if (document.form1.typeIndustry.value == "FICCI Corporate Member") {
			TotalFee=TotalFee+750;
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "FICCI Associate Member") {
			TotalFee=TotalFee+900;	
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "Non Member") {
			TotalFee=TotalFee+1000;	
			cur="INR";
			}
			
      }
	  
	  if(document.form1["category[]"][0].checked == true && document.form1["category[]"][1].checked == true && document.form1["category[]"][2].checked==true && document.form1["category[]"][3].checked==true && document.form1["category[]"][4].checked==true){
		if (document.form1.typeIndustry.value == "FICCI Corporate Member") {
			TotalFee=2625;
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "FICCI Associate Member") {
			TotalFee=3150;	
			cur="INR";
			}
		if (document.form1.typeIndustry.value == "Non Member") {

			TotalFee=3500;	
			cur="INR";
			}
			
	  
	  }

	  if (document.form1.typeIndustry.value == "Non Member") {  
	  document.form1.MembershipNo.disabled="disabled";
	  document.form1.MembershipNo.placeholder="Membership No";
	}
	}
else{	
	  
			document.form1.typeIndustry.value="Non Member" ; 
			document.form1["category[]"][0].checked=true;
			document.form1["category[]"][1].checked=true;
			document.form1["category[]"][2].checked=true;
			document.form1["category[]"][3].checked=true;
			document.form1["category[]"][4].checked=true;
			document.form1["category[]"][5].checked=true;
			document.form1.MembershipNo.disabled="disabled";
			document.form1.MembershipNo.placeholder="Membership No";
			

			TotalFee=65;	
			cur="$";
			
	
}	
	
		//alert(TotalFee);
		document.form1.Amount.value=TotalFee;
		if(cur=="$") { document.getElementById("calFee").innerHTML='<strong>Total Fee: </strong>'+cur+' '+TotalFee+' per Delegate';} 
		else {document.getElementById("calFee").innerHTML='<strong>Total Fee: </strong>'+cur+' '+TotalFee+' (+ GST) per Delegate';}
		

	

    
}

    
    
    
    function validate() {

		
	
		if(document.form1.Promocode.value === "")	{
			//alert('Hiii');
		//return false;
	}	else {
		
	var xx1 = jQuery('div.form-group .deletgae').length;
		//alert(xx1);	return false;	
		if(xx1 >1){
			document.getElementById("agree").checked = false;
			$( "#promoalert" ).text( "Promocode is applicable for one delegate only." ).css("color", "red");	
			return false;
		} else {

	$( "#promoalert" ).text( "" ).css("color", "white");		
	var promo =document.form1.Promocode.value;
	var flag = 0;
    $.ajax({
           	type: "get",
				url: "/check_promo",
				data: "promo=" + promo,
				success: function(data){
					console.log(data);
					
					if(data.status === 'success'){
						// successpromo();
							document.getElementById("agree").checked = false;
	$( "#promoalert" ).text( "Invalid Promocode !!!" ).css("color", "red");	
	document.form1.Promocode.value= promo;
	return false;
	//successpromo(e)
					} else { $( "#promoalert" ).text( "Verified !!!" ).css("color", "Green");}
				
				},
		});
		
	} 
	}


        if(document.form1["category[]"][0].checked == false && document.form1["category[]"][1].checked == false && document.form1["category[]"][2].checked==false && document.form1["category[]"][3].checked==false && document.form1["category[]"][4].checked==false){
            alert("Please provide your interest for registration.")
            document.form1["category[]"][0].focus()
            return false;

        }


		if(document.form1["b_int[]"][0].checked == false && document.form1["b_int[]"][1].checked == false && document.form1["b_int[]"][2].checked==false && document.form1["b_int[]"][3].checked==false && document.form1["b_int[]"][4].checked==false && document.form1["b_int[]"][5].checked == false && document.form1["b_int[]"][6].checked == false && document.form1["b_int[]"][7].checked==false && document.form1["b_int[]"][8].checked==false && document.form1["b_int[]"][9].checked==false && document.form1["b_int[]"][10].checked == false && document.form1["b_int[]"][11].checked == false && document.form1["b_int[]"][12].checked==false && document.form1["b_int[]"][13].checked==false && document.form1["b_int[]"][14].checked==false){
            alert("Please provide Business Category.")
            document.form1["b_int[]"][0].focus()
            return false;

        }

       
 if (document.form1.typeIndustry.value == "") {
            alert("Please select registration category.")
            document.form1.typeIndustry.focus()
            return false;
        }
       
        if (document.form1.typeIndustry.value == "FICCI Corporate Member" || document.form1.typeIndustry.value == "FICCI Corporate Member") {
            if (document.form1.MembershipNo.value == "") {
                alert("Please enter the membership no.")
                document.form1.MembershipNo.focus()
                return false;
            }
        }





        if (document.form1.Name1.value == "") {
            alert("Please enter the Name of the Delegate")
            document.form1.Name1.focus()
            return false;
        }
        if (document.form1.Designation1.value == "") {
            alert("Please enter the Designation of the Delegate")
            document.form1.Designation1.focus()
            return false;
        }
        if (document.form1.Organisation.value == "") {
            alert("Please enter the Name of the Organisation")
            document.form1.Organisation.focus()
            return false;
        }
        if (document.form1.Address.value == "") {
            alert("Please enter the Address")
            document.form1.Address.focus()
            return false;
        }
      
        if (document.form1.country.value == "") {
            alert("Please select your Country")
            document.form1.country.focus()
            return false;
        }

        
        if(document.form1.country.value=="IN:India")
        {
			if (document.form1.State.value == "") {
				alert("Please select your State")
				document.form1.State.focus()
				return false;
				}	
		}

  if (document.form1.City.value == "") {
            alert("Please enter the City")
            document.form1.City.focus()
            return false;
        }
  
    if (document.form1.ZipCode.value == "") {
            alert("Please enter the Pin/Zip/Postal Code")
            document.form1.ZipCode.focus()
            return false;
        }
        
     if (document.form1.gstny.value == "") {
            alert("Do you have a registered GSTN")
            document.form1.gstny.focus()
            return false;
        }
        
          if (document.form1.gstny.value == "Yes") {
            
				if(document.form1.GSTNumber.value=="")
				{
					alert("Please enter your GST Number")
					document.form1.GSTNumber.focus()
					return false;
				}
        }
        
        
        if (document.form1.Telephone.value == "") {
            alert("Please enter the Telephone Number")
            document.form1.Telephone.focus()
            return false;
        }
        if (document.form1.Mobile.value == "") {
            alert("Please enter the Mobile Number")
            document.form1.Mobile.focus()
            return false;
        }
       // if (document.form1.OfficialEmail.value == "") {
           // alert("Please enter your Email address")
          //  document.form1.OfficialEmail.focus()
          //  return false;
       // }

	   



    }


function statechange()
{
	
	
	if(document.form1.country.value=="India")
	{
	  document.form1.State.disabled=false;
	}
	else
	{
		document.form1.State.disabled=true;
		document.form1.State.value="";
	}
}
function statechange1()
{
	
	
	if(document.form1.gstny.value=="YES")
	{
	  document.form1.GSTNumber.disabled=false;
	}
	else
	{
		document.form1.GSTNumber.disabled=true;
		document.form1.GSTNumber.value="";
	}
}

    var index = 2;
    function insertRow() {
        var table = document.getElementById("myTable");
        var row = table.insertRow(table.rows.length);


        var cell0 = row.insertCell(0);
        var t0 = document.createElement("input");
        t0.id = "Name" + index;
        t0.name = "Name" + index;
        t0.setAttribute("type", "text");
		t0.setAttribute("class", "form-control");
        cell0.appendChild(t0);


        var cell1 = row.insertCell(1);
        var t1 = document.createElement("input");
        t1.id = "Designation" + index;
        t1.name = "Designation" + index;
        t1.setAttribute("type", "text");
		t1.setAttribute("class", "form-control");
        cell1.appendChild(t1);


        document.form1.dynamicText.value = index;

        index++;

    }
	

	function check_promo()
	{
		if(document.form1.Promocode.value === "")	{
			//alert('Hiii');
		//return false;
		document.getElementById("agree").checked = true;
		$("#savedata").prop('disabled', false);
		$( "#promoalert" ).text( "" ).css("color", "Green");
		}	 
		else {
			var xx1 = jQuery('div.form-group .deletgae').length;
			//alert(xx1);	return false;	
			if(xx1 >1){
				document.getElementById("agree").checked = false;
				$( "#promoalert" ).text( "Promocode is applicable for one delegate only." ).css("color", "red");	
				return false;
			} else {
				var promo =document.form1.Promocode.value;
				$.ajax({
				type: "get",
					url: "/check_promo",
					data: "promo=" + promo,
					success: function(data){
							//console.log(data);
							
							if(data.status === 'success'){
								// verfypromo();
							document.getElementById("agree").checked = false;
							$( "#promoalert" ).text( "Invalid Promocode !!!" ).css("color", "red");	
							document.form1.Promocode.value= promo;
							$("#savedata").prop('disabled', true);
							} else { 
								$("#savedata").prop('disabled', false);
								$( "#promoalert" ).text( "Verified !!!" ).css("color", "Green");	}
					},
				});
			}
		}
}			 


	function verfypromo()
{
	//e.preventDefault();
	//document.getElementById("agree").checked = false;
	$( "#promoalert" ).text( "Invalid Promocode !!!" ).css("color", "red");	
	document.form1.Promocode.value= promo;
	$("#savedata").prop('disabled', true);
}
</script>
