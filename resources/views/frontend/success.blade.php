@include('frontend.layout.header')
	<br />
	<section id="success">
		<div class="container">
			<div class="row justify-content-md-center">
				<div class="col-lg-10">
					<div class="card shade text-center">
						<img src="/img/success.jpg" class="img-fluid mx-auto d-block" />
						
						<h1 style="font-size:30px; margin:10px 0;"> Dear @if(null !==session()->get('dname'))
						 {{ucfirst(session()->get('dname'))}}, @else Member, @endif <br />

						Thanks for your registration. </h1>

						<p> We shall shortly send B2B meetings & login details to you. </p>

						<p> For further details, you  may contact : <br />
						<strong> Registrations :</strong> <a href="mailto:frames.registration@ficci.com"> frames.registration@ficci.com </a> <br />
						<strong> Programme :</strong>  <a href="mailto:amit.tyagi@ficci.com"> amit.tyagi@ficci.com </a> <br />
						<strong> Sponsorship's/E-stalls/Advertisement :</strong>  <a href="mailto:samir.kumar@ficci.com"> samir.kumar@ficci.com </a> <br />

						<p> Team FICCI FRAMES </p>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section id="partners" class="section-with-bg">
		<div class="container-fluid">
			<div class="section-header">
				<h2>Partners</h2>
			</div>
		<center>	<div class="col-lg-3 col-md-4 col-xs-6" style="padding-bottom:10px;">
					<a href="https://www.startv.com/" target="_blank" class="supporter-logo" style="border-bottom:none;border-right:none;"> 
						<p> <strong> Convention partner </strong> </p> <img src="{{asset('img/sponsor/star.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div> </center>
			<div class="row no-gutters supporters-wrap clearfix">
				
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.intel.in/content/www/in/en/homepage.html" target="_blank"  class="supporter-logo"> 
						<p> <strong> Diamond partner & BAF Awards  Co-Presenter </strong></p><img src="{{asset('img/sponsor/intel.jpg')}}" class="img-fluid" alt="">   
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.netflix.com/in/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Gold partner </strong> </p> <img src="{{asset('img/sponsor/netflix.jpg')}}" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.motionpictures.org/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate partner </strong> </p>  <img src="{{asset('img/sponsor/mpa.jpg')}}" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.discovery.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate Partner </strong> </p>  <img src="{{asset('img/sponsor/discovery.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.ufomoviez.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Cine Media Partner </strong> </p> <img src="{{asset('img/sponsor/ufo.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.mediabrief.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p> <img src="{{asset('img/sponsor/media_brief.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.medianews4u.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="{{asset('img/sponsor/media_news.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				

				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.adgully.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="{{asset('img/sponsor/adgully.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>

				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.mxplayer.in" target="_blank"  class="supporter-logo"> 
						<p> <strong> OTT Partner </strong> </p>  <img src="{{asset('img/sponsor/mx-player.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>

			</div>
		</div>
	</section>  
	
@include('frontend.layout.footer')

