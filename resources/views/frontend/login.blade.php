<!-- Css Styles -->
<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
	
	<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
	<style>.card{padding:20px !important;}  a:hover{color:blue;}</style>
</head>

<body id="page-top">
 
	<section class="register_form">
	
	<div class="row justify-content-md-center" style="margin-right: 0px;"> 

		<div class="col-sm-6 login">
				 <img src="{{asset('img/loginpage.jpeg')}}" class="img-fluid" alt=""> 
		</div>

		<div class="col-sm-6 card login">
        
					<h2 class="text-center"> LOGIN 
					<!-- @if(isset(Auth::user()->email)) {{Auth::user()->email}} @endif --> </h2>
					<div class="title-border"><span></span></div>
                  		  <!--  To Show Alert Message -->
							@if (count($errors) > 0)
								<div class="alert alert-danger">
									<strong>Whoops!</strong> There were some problems with your input.<br><br>
									<ul> @foreach ($errors->all() as $error)<li>{{ $error }}</li>@endforeach </ul>
								</div>
							@endif

								@if ($message = Session::get('error'))
								<div class="alert alert-danger">
									<button type="button" class="close" data-dismiss="alert">×</button>
									<strong>{{ $message }}</strong>
								</div>
								@endif

								@if ($message = Session::get('success'))
								<div class="alert alert-success alert-block">
									<button type="button" class="close" data-dismiss="alert">×</button>
									<strong>{{ $message }}</strong>
								</div>
								@endif
							<!--  / To Show Alert Message -->
				<form name="form1" method="post" action="/delegate/login"   class="contact100-form validate-form">
						@csrf
                            <div class="row">
								<div class="col-sm-12 form-group required">
									<label class="bold"> Email ID </label>
									<input name="email" class="form-control" maxlength="255" type="email" value="{{ old('email') }}"  placeholder="Email ID*" id="email" required="required" />
                                    @error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
                                </div>
								<div class="col-sm-12 form-group required">
									<label class="bold"> Password </label>
									<input name="password" class="form-control" maxlength="255" type="text" value=""  placeholder="Password*" id="password"  required="required" />
                                    @error('password')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
                                </div>
							</div>	
							<div class="submit">
								<input type="submit" name="Submit" value="Login"  id="savedata" class="btn w-100 primary-btn"  > 
								
							</div>
						</form>		
						<center><a href="#">Forgot Password?</a>	</center>		
						<p> For further details, you  may contact : <br />
						<strong> Registrations :</strong> <a href="mailto:frames.registration@ficci.com"> frames.registration@ficci.com </a> <br />
						<strong> Programme :</strong>  <a href="mailto:amit.tyagi@ficci.com"> amit.tyagi@ficci.com </a> <br />
						<strong> Sponsorship's/E-stalls/Advertisement :</strong>  <a href="mailto:samir.kumar@ficci.com"> samir.kumar@ficci.com </a> <br />
            </div> <!-- Row -->
      </div>
	
	</section>
	
	
	
	<section id="partners" class="section-with-bg">
		<div class="container-fluid">
			<div class="section-header">
				<h2>Partners</h2>
			</div>
		<center>	<div class="col-lg-3 col-md-4 col-xs-6" style="padding-bottom:10px;">
					<a href="https://www.startv.com/" target="_blank" class="supporter-logo" style="border-bottom:none;border-right:none;"> 
						<p> <strong> Convention partner </strong> </p> <img src="{{asset('img/sponsor/star.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div> </center>
			<div class="row no-gutters supporters-wrap clearfix">
				
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.intel.in/content/www/in/en/homepage.html" target="_blank"  class="supporter-logo"> 
						<p> <strong> Diamond partner & BAF Awards  Co-Presenter </strong></p><img src="{{asset('img/sponsor/intel.jpg')}}" class="img-fluid" alt="">   
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.netflix.com/in/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Gold partner </strong> </p> <img src="{{asset('img/sponsor/netflix.jpg')}}" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.motionpictures.org/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate partner </strong> </p>  <img src="{{asset('img/sponsor/mpa.jpg')}}" class="img-fluid" alt="">
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.discovery.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Associate Partner </strong> </p>  <img src="{{asset('img/sponsor/discovery.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.ufomoviez.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Cine Media Partner </strong> </p> <img src="{{asset('img/sponsor/ufo.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.mediabrief.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p> <img src="{{asset('img/sponsor/media_brief.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.medianews4u.com/" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="{{asset('img/sponsor/media_news.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>
				

				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.adgully.com" target="_blank"  class="supporter-logo"> 
						<p> <strong> Online Media Partner </strong> </p>  <img src="{{asset('img/sponsor/adgully.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>

				<div class="col-lg-3 col-md-4 col-xs-6">
					<a href="https://www.mxplayer.in/" target="_blank"  class="supporter-logo"> 
						<p> <strong> OTT Partner </strong> </p>  <img src="{{asset('img/sponsor/mx-player.jpg')}}" class="img-fluid" alt=""> 
					</a>
				</div>

			</div>
		</div>
	</section>  
	
	
@include('frontend.layout.footer')


