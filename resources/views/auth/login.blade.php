@extends('layouts_admin.app')
@section('content')

<style>
body { background:#f3f3f4; overflow:hidden; }
#page-wrapper { margin:0px !important;}
.footer { display:none; }
.logo { width:300px; margin:30px auto; padding:20px 20px; background:#fff; }
</style>

<div class="admin_body loginscreen animated fadeInDown">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-4 col-md-6">				
				<div class="logo"><img src="/img/logo.jpg" class="img-fluid mx-auto d-block " /> </div>

				@if ($message = Session::get('error'))
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif

@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{{ $message }}</strong>
</div>
@endif
				<div class="card">
					<h2 class="text-center"> {{ isset($url) ? ucwords($url) : ""}} {{ __('Login') }} </h2>
					<hr />
					<!--<form method="POST" action="{{ route('login') }}">   -->
					 
                        <form method="POST" action='{{ url("/admin/login") }}' aria-label="{{ __('Login') }}">
                      
						@csrf
						<div class="form-group">
							<label for="user_name" class="">Email</label>
							<div class="">
								<input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="user_name" autofocus>
								@error('email')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="">{{ __('Password') }}</label>
							<div class="">
								<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
								@error('password')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group mb-0">
							<div class="">
								<button type="submit" class="btn btn-primary w-100 primary">
									{{ __('Login') }}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
