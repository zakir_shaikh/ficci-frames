
	<!-- Navigation -->
	<nav class="navbar-default navbar-static-side" role="navigation">
		<div class="sidebar-collapse" id="sidetree">
			<ul class="treeview nav metismenu" id="tree">
				<li class="nav-header">
					<div class="dropdown profile-element text-center"> <img src="/img/logo.jpg" class="img-fluid" />  </div>
					<div class="logo-element"> <img src="/img/favicon.png" class="img-fluid" /> </div>
				</li>
				<li> <div class="hitarea expandable-hitarea"><a href="{{url('/admin')}}"><i class="fa fa-tachometer" aria-hidden="true"></i> <span> Dashboards </span></a></div> </li>
                <li> <div class="hitarea expandable-hitarea"><a href="{{url('/admin/paid/registration')}}"><i class="fa fa-users"></i>Paid Registration </a></div> </li>  
				<li> <div class="hitarea expandable-hitarea"><a href="{{url('/admin/unpaid/registration')}}"><i class="fa fa-users"></i>Unpaid Registration </a></div> </li> 
				<li> <div class="hitarea expandable-hitarea"><a href="{{url('/admin/promocodes')}}"><i class="fa fa-users"></i>Promo Codes </a></div> </li>
                
				

				<!--<li class="expandable">
					<div class="hitarea expandable-hitarea"><i class="fa fa-user-o" aria-hidden="true"></i> <span>  Administration </span> <i class="fa fa-angle-down" aria-hidden="true"></i> </div>
					<ul class="category" style="display:none;">
						<li><a href="/admin/user">  User </a></li>
						<li><a href="/admin/change_password"> Change Password </a></li>
					</ul>
				</li> -->
			</ul>
		</div>
	</nav>

	<div class="nav_footer">
		<div class="nav-fold">
			<span class="pull-left"> <img src="/img/avatar.png" alt="Avatar" class="w-40 img-circle"/> </span>
			<span class="hidden-folded p-x">
				<span class="white"> Ficci</span>
				<small class="block"><i class="fa fa-circle text-success m-r-sm"></i>Online</small>
			</span>
		</div>
	</div>


