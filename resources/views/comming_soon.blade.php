
	<body style="padding:0px; margin:0px;">
		<section style="padding:0px; margin-bottom:0px;">
			<img src="https://vzcompetitions.volzero.com/img/little_bigloo/registration_confirmation.png" style="width:100%" />
			<div class="container" style="padding:20px 100px; background:#fff; margin-bottom:0px;">
				<span style="display:block; background:#a8a9ad; height:2px; margin:5px auto 20px auto;"></span>				
				<p style="font-family:Arial,sans-serif;color:#666;"> Hello Sanaullah Khan, </p>		 	
				<p style="font-family:Arial,sans-serif;color:#666;"> Congratulations! </p>
				<p style="font-family:Arial,sans-serif;color:#666;"> Your registration process is complete and your unique code for the competition is </p>
				<p style="padding:2px;line-height:20px;text-align:center;font-family:Arial,sans-serif;font-size:22px;background:#333;width:166px;display:inline-block;">
					<span style="padding:4px 10px 4px 10px;display:block; background:#fff; color:#23bfea;"> VZPTABCD01 </span>
				</p>
				<p style="font-family:Arial,sans-serif;color:#666;"> We request you to mention your team code on the top right corner of your sheet during submission, and also for sending us any queries. </p>
				<p style="font-family:Arial,sans-serif;color:#666;"> The submission must be sent to <a href="mailto:submitlittlebigloo@volzero.com" style="text-decoration:none; color:#000;">submitlittlebigloo@volzero.com</a> and CC the email to <a href="mailto:submitlittlebigloo@gmail.com" style="text-decoration:none; color:#000;">submitlittlebigloo@gmail.com</a> <span style="text-decoration:none; color:#000;">before 11:59 pm,  31st July 2020.</span> </p>
				<p style="font-family:Arial,sans-serif;color:#666;"> If you have any questions just shoot us an email at <a href="mailto:questions@volzero.com" style="text-decoration:none; color:#000;">questions@volzero.com</a>with “The Little Big Loo FAQ”” as subject , we are always there to help.</p>
				<p style="font-family:Arial,sans-serif;color:#666;"> Best of Luck! We look forward to receiving your design entry. </p>
				<p style="font-family:Arial,sans-serif;color:#666;">Thank you,<br />
				Team Volume Zero</p>
				<br />
				<div style="background:#f3f6f6; width:100%; padding:15px;">
					<p style="margin:0px; line-height:normal;font-family:Arial,sans-serif;">
						<span style="display:inline-block; width:39%;margin-bottom:8px;"> Team Member 1 - Leader </span> 
						<span style="display:inline-block; width:30%;margin-bottom:8px;"> Email </span> 
						<span style="display:inline-block; width:30%;margin-bottom:8px;"> Contact </span>
						<span style="display:block;"></span>					
						<span style="display:inline-block; width:39%;margin:0px;"> Sanaullah Khan </span> 
						<span style="display:inline-block; width:30%;margin:0px;"> khan@abcdesigns.in </span>
						<span style="display:inline-block; width:30%;margin:0px;"> +91 8080492427 </span>
					</p>
					<span style="display:block; background:#d1d2d4; height:2px; margin:10px auto;"></span>
					<p style="margin:0px;line-height:normal;font-family:Arial,sans-serif;"> <span style="margin-bottom:8px;display:block"> Team Member 2 </span>				
						<span style="display:inline-block; width:39%;"> Mohammad Zakir </span> 
						<span style="display:inline-block; width:30%;"> zakir@abcdesigns.in </span>
						<span style="display:inline-block; width:30%;"> +91 8080492427  </span>
					</p>
					<span style="display:block; background:#d1d2d4; height:2px; margin:10px auto;"></span>
					<p style="margin:0px; line-height:normal;font-family:Arial,sans-serif;">  <span style="margin-bottom:8px;display:block"> Team Member 3 </span>			
						<span style="display:inline-block; width:39%;"> Hitesh Kanavale </span> 
						<span style="display:inline-block; width:30%;"> hitesh@abcdesigns.in </span>
						<span style="display:inline-block; width:30%;"> +91 8080492427  </span>
					</p>
				</div>
				<br />
				<span style="display:block; background:#a8a9ad; height:2px; margin:5px auto;"></span>
			</div>
			<img src="https://vzcompetitions.volzero.com/img/little_bigloo/registration_logo.png" style="width:100%"  /> 
		</section>
	</body>
