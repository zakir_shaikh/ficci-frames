


<div style="background:#efefef;padding:10px;width:420px;margin:10px auto;">
	<table bgcolor="#fff" width="450" align="center" style="padding:10px;">
		<tr>
			<td>
				<!-- <img src="https://eventregistration.in/img/logo.jpg" width="240px;" />  <br />  <br />  -->
				<br />
				<p style="font-family:Arial,sans-serif;line-height:22px;color:#666;font-size:16px;"> Dear Dr. {{ $data1['name'] }}, </p>
				<p style="font-family:Arial,sans-serif;line-height:22px;color:#666;font-size:16px;"> Your Registration has been APPROVED. </p> </br>

                <p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;"> For logging in at https://ve.vhhe.in Your User ID is <b>{{ $data1['email'] }} </b> and Password is <b>{{ $data1['digits'] }} </b>. Please remember it or keep handy for use time-to-time from July 07-11, 2020.   </p>
					<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;"> <b>Visitors' Guide </b> for using our virtual platform to meet with exhibitors and to attend webinars is as follows: </p>
				<br>
				<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;padding-left:5px;">• To reach in virtual platform visit virtual platform ve.vhhe.in </b>
				<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;padding-left:5px;">• Use your user id & password.</b>
				<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;padding-left:5px;">• Your Virtual convention centre journey starts now.</b>
				<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;padding-left:5px;">• You have to click on first floor and you will reach in 5 exhibition halls area like AYUSH, Pharmaceutical, Medical Devices, Hygiene & Sanitization and Medical Textiles.</b>
				<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;padding-left:5px;">• Enter the hall of your choice and navigate through different booths, and choose a booth to start your interaction.</b>
				<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;padding-left:5px;">• Once you arrive at the stall, you will find the company exhibits like video, product posters in brochure stands.</b>
				<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;padding-left:5px;">• You can interact with exhibitior through video chat and text chat.</b>
				<p style="font-family:Arial,sans-serif;line-height:23px;font-size:16px;color:#666;width:100%;padding-left:5px;">• Exhibitor will get Alert in his system and immediately speak/ chat with you.</b>

				
				<p style="font-family:Arial,sans-serif;line-height:22px;font-size:16px;color:#666;width:100%;font-weight:bold;">Similar way you can visit to other company's hall/ stall for your pre-fixed or walk in meetings.</b>
				<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;padding-left:5px;">For attending webinars, organiser will provide you webinars schedule and you can click on session hall 1 for live sessions and session hall 2 for archives of sessions.</b>
				<p style="font-family:Arial,sans-serif;line-height:20px;font-size:16px;color:#666;width:100%;padding-left:5px;">Location of the webinar is in the ground floor of the virtual convention centre marked fascia as Session I and Session II.</b>

				<p style="font-family:Arial,sans-serif;line-height:22px;font-size:16px;color:#666;width:100%;font-weight:bold;">For any difficulty in login you may contact Live information desk or technical help or email to sudhanshu.gupta@ficci.com</b>

				<p style="font-family:Arial,sans-serif;line-height:22px;font-size:16px;color:#666;font-weight:bold;">  TEAM VHHE 2020 </p>
				
				<br> <br> <br>

				<p style="font-family:Arial,sans-serif;line-height:17px;font-size:11px;color:#666;">  This message is intended for the addressee only and may contain confidential or privileged information. </p>

				<p style="font-family:Arial,sans-serif;line-height:17px;font-size:11px;color:#666;">  The communication is the property of FICCI and its affiliates and may contain copyright material or intellectual property of FICCI and/or any of its related entities or of third parties. If you are not the intended recipient of the communication or have received the communication in error, please notify the sender or FICCI immediately, return the communication (in entirety) and delete the communication (in entirety and copies included) from your records and systems. Unauthorized use, disclosure or copying of this communication or any part thereof is strictly prohibited and may be unlawful. </p>
				
			</td>
		</tr>
	</table>
</div>

