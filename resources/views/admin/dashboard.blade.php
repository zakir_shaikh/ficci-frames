@extends('layouts_admin.app')
@section('content')
<?php use App\Http\Controllers\AdminController; ?>
	<div class="wrapper wrapper-content">
		<div class="row text-center">

			<div class="col-lg-6">
				<div class="ibox ">
					<div class="ibox-title">
						<h5>Paid Registration</h5>
					</div>
					<div class="ibox-content">
						<h1 class="no-margins">{{$total_paid}}</h1>
					</div>
				</div>
            </div>


			<div class="col-lg-6">
				<div class="ibox ">
					<div class="ibox-title">
						<h5>Unpaid Registration</h5>
					</div>
					<div class="ibox-content">
                    <h1 class="no-margins">{{$unpaid}}</h1>
					</div>
				</div>
			</div>

		</div> <!-- Row -->
		<div class="row">
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5>Total Paid Registration</h5>
						<div class="ibox-tools"> <a class="collapse-link">
							<a href="{{url('/admin/paid/registration/export/excel')}}">
								<button class='btn btn-info'> <i class="fa fa-plus" aria-hidden="true"></i> Download Excel </button>
							</a>
							&nbsp;
							<i class="fa fa-chevron-up"></i> </a>
                         </div>
                    </div>
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<table class="table table-hover table-bordered table-striped margin bottom">
									<thead>
                                        <tr>
											<th><a href="#" class="desc"> # </a></th>
                                            <th><a href="#">Date</a></th>
											<th><a href="#">RegId</a></th>
                                            <th><a href="#">Email</a></th>
                                            <th><a href="#">Contact</a></th>
											<th><a href="#">Nationality</a></th>
											<th><a href="#">Type Industry</a></th>
                                            <th><a href="#">Delegates</a></th>
											<th><a href="#">Organisation</a></th>
											<th><a href="#">No of Delegates</a></th>
											<th><a href="#">Amount</a></th>
											<th><a href="#">Trans. ID</a></th>
											<th><a href="#">Invoice No</a></th>
											<th><a href="#">Country</a></th>
											<th><a href="#">City</a></th>
									    </tr>
									</thead>
									<tbody>
                                        @foreach($todays as $value)
                                        <tr>
                                              <td>{{ $i++ }}</td>
                                              <td> {{ date('d-m-Y | H:i:A',strtotime($value->created_at))}}  </td>
                                              <td> {{$value->RegId}} </td>
                                              <td> {{ $value->email }}  </td>
                                              <td> {{ $value->Mobile }}  </td>
                                              <td> {{ $value->nationality }}  </td>
											  <td> {{ $value->typeIndustry }}  </td>
											  <td> <?php $delegate = AdminController::getdelegates($value->id); $d=1; ?>
											  			@foreach($delegate as $del)
														  <p><b>{{$d}}- {{$del->name}} <b> </p>
														  <?php $d++;?>
														  @endforeach
											   </td>
											  <td> {{ $value->Organisation }}  </td>
											  <td> {{ $value->quantity }}  </td>
											  <td> {{ $value->Amount }} + GST </td>
											  <td> {{ $value->txnId }}  </td>
											  <td> {{ $value->invoice_no }}  </td>
											  <td> {{ $value->country }}  </td>
											  <td> {{ $value->city }}  </td>
                                             
                                           <!--     <td class="actions">

                                          <a onclick="return confirm('Are you sure?')" href='{{url("/admin/value/delete/$value->id")}}' class="btn btn-danger btn-sm"><i class="fa fa fa-trash-o"></i></a>
                                              </td>-->
                                          </tr>
                                          @endforeach

									</tbody>
								</table>
								<div class="pull-right"><?php echo $todays->render(); ?> </div>
							</div>
						</div>
					</div>
				</div> <!-- Row -->
			</div>
		</div>
		@include('layouts_admin.admin_footer')
	</div> <!-- page-wrappe -->
@endsection
