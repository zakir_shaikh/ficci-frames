@extends('layouts_admin.app')
@section('content')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-8">
        <h2> Add Users </h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"> <a href="dashboard"> Home </a>  </li>
            <li class="breadcrumb-item"> Administration </li>
            <li class="breadcrumb-item"> Add New Users </li>
        </ol>
    </div>
    <div class="col-sm-4">
        <div class="title-action">
            <a href="/admin/user"><button class='btn btn-info'> List Users</button></a>
        </div>
    </div>
</div>

<div class="wrapper wrapper-content">
	<div class="animated fadeInUp">
		<div class="row justify-content-md-center">			
			<div class="col-lg-12">
				<div class="ibox">
					<div class="ibox-title">
						<h5 class="mb-0"> <a href="#" class="btn back_btn" title="Back"> <i class="fa fa-angle-left" aria-hidden="true"></i> </a> Add Users</h5>
						<div class="ibox-tools">
							<a class="collapse-link"> <i class="fa fa-chevron-up"></i> </a>
						</div>
					</div>
					<div class="ibox-content">
						<div class="row justify-content-md-center">			
							<div class="col-lg-7 col-md-8">
								<div class="card">
									<form>
										<div class="row">
											<div class="col-lg-6 form-group required">
												<label for="UserFirstName">First Name</label>
												<input name="data[User][first_name]" class="form-control" maxlength="100" type="text" id="UserFirstName" required="required">
											</div>
											<div class="col-lg-6 form-group required">
												<label for="UserLastName">Last Name</label>
												<input name="data[User][last_name]" class="form-control" maxlength="100" type="text" id="UserLastName" required="required">
											</div>
										</div>
										<div class="form-group required">
											<label for="UserEmailAddress">Email Address</label>
											<input name="data[User][email_address]" class="form-control" maxlength="100" type="text" id="UserEmailAddress" required="required">
										</div>
										<div class="row">
											<div class="col-lg-6 form-group required">
												<label for="UserUsername">Username</label>
												<input name="data[User][username]" class="form-control" maxlength="100" type="text" id="UserUsername" required="required">
											</div>
											<div class="col-lg-6 form-group required">
												<label for="UserPassword">Password</label>
												<input name="data[User][password]" class="form-control" type="password" id="UserPassword" required="required">
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6 form-group required">
												<label for="UserGroupId">Group</label>
												<select name="data[User][group_id]" class="form-control" id="UserGroupId" required="required">
													<option value="">--Choose Group--</option>
													<option value="1">SuperAdmin</option>
													<option value="2">Managers</option>
												</select>
											</div>
											<div class="col-lg-6 form-group required">
												<label for="UserIsActive">Status</label>
												<select name="data[User][is_active]" class="form-control" id="UserIsActive" required="required">
													<option value="0">In Active</option>
													<option value="1" selected="selected">Active</option>
												</select>
											</div>
										</div>
										<div class="submit">
											<input class="btn btn-primary btn-lg btn-block" type="submit" value="Submit">
										</div>
									</form>
								</div>
								<br />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts_admin.admin_footer')
</div> <!-- page-wrappe -->
@endsection


