<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $guard = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'RegId','email','nationality', 'typeIndustry', 'MembershipNo','Organisation','Address','password_view','country','created_at','state','city','zipcode','Mobile','Website','personal_profile','current_business_interest',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function delegates()
    {
        return $this->hasMany('App\Delegates');
    }

    public function fdays()
    {
        return $this->hasMany('App\Fdays');
    }    

    public function businessctg()
    {
        return $this->hasMany('App\Businessctg');
    }
}
