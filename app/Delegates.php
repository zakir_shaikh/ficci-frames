<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delegates extends Model
{
    protected $fillable = [
        'regid','name','designation',
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
