<?php

namespace App\Exports;

use App\Feedback_ans;
use App\eedback_questions;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class FeedbackExport implements FromCollection,WithHeadings
{
	
	public function __construct(string $fid)
    {
        $this->fidd = $fid;
        
    }
	
    public function collection()
    {
		$fidd = $this->fidd;
        $users =Feedback_ans::leftjoin('users', 'users.id', '=', 'feedback_ans.u_id')
        //->leftjoin('feedback_questions', 'feedback_questions.id', '=', 'feedback_ans.fq_id')
	    ->select('feedback_ans.session_id','users.name','fq_id','f_ans','q2','ans2','q3','ans3','q4','ans4','q5','ans5','feedback_ans.created_at')
		->where('feedback_ans.session_id',$fidd)
		->orderby('feedback_ans.created_at', 'desc')
        ->get();
		
		
        return $users;
    }


    public function headings(): array
    {
        return [
            'Feedback number',
            'user',
            'Question 1',
            'Rating',
			'Question 2',
            'Rating',
			'Question 3',
            'Rating',
			'Question 4',
            'Rating',
			'Question 5',
            'Rating',
            'Date',

		];
    }
}
