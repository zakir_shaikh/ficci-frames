<?php

namespace App\Exports;


use App\User;
use App\Event_vistis;
use App\Activity_status;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
class ActivityExport implements FromCollection
{
    public function __construct(string $fromdate, string $todate)
    {
        $this->fromdate = $fromdate;
        $this->todate = $todate;
    }

    public function collection()
    {
        //print_r($this->year); print_r($this->date); exit;
        $from =$this->fromdate;
        $to = $this->todate;
// whereBetween('reservation_from', [$from, $to])
        if($this->fromdate !=''){
            $users =Event_vistis::leftjoin('users', 'event_vistis.user_id', '=', 'users.id')
            ->leftjoin('activity_statuses', 'event_vistis.room_type', '=', 'activity_statuses.sid')
		->select('users.name','activity_statuses.name AS stname','event_vistis.created_at')
        ->whereBetween('event_vistis.created_at', [$from, $to])
        ->orderby('event_vistis.created_at', 'asc')
        ->get();

        }else{
            $users =Event_vistis::leftjoin('users', 'event_vistis.user_id', '=', 'users.id')
            ->leftjoin('activity_statuses', 'event_vistis.room_type', '=', 'activity_statuses.sid')
		->select('users.name','activity_statuses.name AS stname','event_vistis.created_at')
       ->orderby('event_vistis.created_at', 'asc')
        ->get();
        }

        return $users;
    }

    public function headings(): array
    {
        return [
            'Name',
            'Activity',
            'Date',
		];
    }
}
