<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Businessctg extends Model
{
    protected $fillable = [
        'regid','bctg',
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
