<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Countries;
use App\Cities;
use App\States;
use App\User;

use Illuminate\Support\Facades\DB;
class StaticfunctionController extends Controller
{


    public static function user_name($id)
    {
        $usern =DB::table('users')->where('id',$id)->first();
        $uname =$usern->name;
         return $uname;
     }

     public static function state_name($id)
    {
        $state =DB::table('states')->where('state_id',$id)->first();
         return $state;
     }

     public static function city_name($id)
    {
        $city =DB::table('cities')->where('city_id',$id)->first();
         return $city;
     }

    public static function country_details($user_id)
    {   $address=DB::table('address_others')->select('country_id')->where('user_id', $user_id)->first();
        if( $address) {
        $country =DB::table('countries')->select('name')->where('county_id',$address->country_id)->first();
        $country1 = $country->name;
        return $country1;
        }
    }


    public static function country_code($user_id)
    {   $address=DB::table('address_others')->select('country_id')->where('user_id', $user_id)->first();
        if($address) {
        $country =DB::table('countries')->select('name','phonecode')->where('county_id',$address->country_id)->first();
         return $country;
        }
    }



    public static function city_list($countryid, $contact, $city1)
    {

        $country =  Countries::select('county_id','sortname','phonecode')->where('county_id',$countryid)->first();

        $phonecode = $country->phonecode;
        $sortname = strtolower($country->sortname);
        $imgpath = '/img/flags-medium/'.$sortname.'.png';
        $cities = Cities ::join('states', 'states.state_id', '=', 'cities.state_id')
        ->select('cities.city_id', 'cities.name')
        ->where('states.country_id', '=', $countryid)
		->orderby('cities.name','asc')->get();
        if(count($cities) > 0) {
            return view('ajax_pages.address', compact('cities','imgpath','phonecode','contact','city1'));}
        else {
           $states =  States::select('state_id','name')->where('country_id', '=', $countryid)->get();
           return view('ajax_pages.address', compact('states','imgpath','phonecode','contact','city1'));
        }
    }











}
