<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\ParticipantsExport;
use App\Exports\PaidparticipnatsExport;
use App\Exports\ActivityExport;
use App\Exports\QuestionExport;
use App\Exports\FeedbackExport;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Fill;
use App\Countries;
use App\Cities;
use App\States;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
class ExportExcelController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 public function __construct()
    {
        //$this->middleware('auth:admin');
    }

    public function total_excel()
    {   if(isset($_GET['from_date']) && $_GET['from_date']!=''){
        $from_date =$_GET['from_date'].' 00:00:00';
        $two_date= $_GET['to_date'].' 23:59:59';
        }
        else{ $from_date ='';
            $two_date='';}

		return Excel::download(new ParticipantsExport($from_date,$two_date), 'unpaid_registration.xlsx');

    }

    public function today_excel()
    {
        if(isset($_GET['from_date']) && $_GET['from_date']!=''){
            $from_date =$_GET['from_date'].' 00:00:00';
            $two_date= $_GET['to_date'].' 23:59:59';
            }
            else{ $from_date ='';
                $two_date='';}

        $tdata = Carbon::today();
        return Excel::download(new PaidparticipnatsExport($from_date,$two_date), 'paid_registration.xlsx');

    }

    public function actvity_excel()
    {
        if(isset($_GET['from_date']) && $_GET['from_date']!=''){
            $from_date =$_GET['from_date'].' 00:00:00';
            $two_date= $_GET['to_date'].' 23:59:59';
            }
            else{ $from_date ='';
                $two_date='';}
        return Excel::download(new ActivityExport($from_date,$two_date), 'Activity.xlsx');

    }

    public function question_excel()
    { return Excel::download(new QuestionExport, 'Questions.xlsx'); }

    public function feedback_excel($fid)
		
    { return Excel::download(new FeedbackExport($fid), 'Feedback.xlsx'); }



}
