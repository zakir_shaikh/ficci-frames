                     <!--       @if($data)
									<div class="row justify-content-md-center" >
										<div class="col-md-6">
											<div class="card shade" id="hidepoll">

												<div class="form-group text-center" id="replace">
													<h2 class="orange"> Q. {{$data->question}} </h2>
													<input type="hidden" name="qid" id="qid" value="{{$data->id}}" >
													<ul class="simple-radio text-left"id="sr1" >
														@foreach($qoptions as $opts)
														<li>
															<input type="radio" name="group"  value="{{$opts->id}}" />
															<div class="check"><span class="no">&nbsp;</span><span class="yes"><i class="fa fa-check-circle"></i></span></div>
															<strong> @if($i==1) A @endif @if($i==2) B @endif @if($i==3) C @endif @if($i==4) D @endif. </strong>{{$opts->options}}

                                                        </li>
                                                        <?php $i++;?>
														@endforeach

													</ul>
												</div>
												<div class="form-group submit">
													<input class="btn btn-primary btn-block login_btn" id="addMoney" type="submit" value="Submit" onclick="save_ans()">
												</div>
                                            </div>

											<br />
										</div>
										<div class="col-md-6" >
											<div class="card shade">
												<div id="chart-example-2"></div>
											</div>
										</div>
                                    </div>

									@else <input type="hidden" name="qid" id="qid" value="0" >
									@endif
                                    </div>
                                -->

               @if($data)
				<div class="card shade" id="poll_modal">
					<div class="form-group text-center">
                        <input type="hidden" name="qid" id="qid" value="{{$data->id}}" >
						<h2 class="orange"> {{$data->question}} </h2>
						<ul class="simple-radio text-left" id="sr1">
							@foreach($qoptions as $opts)
														<li>
															<input type="radio" name="group"  value="{{$opts->id}}" />
															<div class="check"><span class="no">&nbsp;</span><span class="yes"><i class="fa fa-check-circle"></i></span></div>
															<strong> @if($i==1) A @endif @if($i==2) B @endif @if($i==3) C @endif @if($i==4) D @endif. </strong>{{$opts->options}}
                                                        </li>
                                                        <?php $i++;?>
														@endforeach

						</ul>
					</div>
					<div class="form-group submit">
						<input class="btn btn-primary btn-block login_btn" id="addMoney" type="button" value="Submit" onclick="save_ans()">
                    <b id="p_alert"></b>
                    </div>
                </div>
                <script>
                    // simple radio - sets up shop
                    $('.simple-radio li').each(function(index) {
                      if ($('input', this).prop('checked')) {
                        $(this).addClass('checked');
                      }
                    });
                    // simple radio - listen for click
                    $('.simple-radio li').click(function() {
                      $('.simple-radio input').removeAttr('checked');
                      $('input', this).attr('checked', 'checked');
                      $('input', this).prop({
                        checked: true
                      });
                      $('.simple-radio li').removeAttr('class');
                      $(this).addClass('checked');
                    });


                </script>
                @else <input type="hidden" name="qid" id="qid" value="0" >
                @endif
