<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Countries;
use App\Cities;
use App\States;
use App\User;
use App\Businessctg;
use App\Fdays;
use App\Delegates;
use App\Promocodes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Session;
use Mail;
class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {   $i=1;
        if(isset($_GET['name']) || isset($_GET['city']) || isset($_GET['state'])) {
            if($_GET['name']!='' && $_GET['state']!='' && $_GET['city']!='')
            {
                $user = User::where('name','like', "%" . $_GET['name'] . "%")
                ->where('state',$_GET['state'])->where('city',$_GET['city'])->where('invoice_no','!=','')->paginate(40);
            }
            elseif($_GET['state']!='' && $_GET['city']!='')
            {
                $user = User::where('state',$_GET['state'])->where('city',$_GET['city'])->where('invoice_no','!=','')->paginate(100);
            }
            elseif($_GET['state']!='')
            {
                $user = User::where('state',$_GET['state'])->where('invoice_no','!=','')->paginate(100);
            }
            elseif($_GET['name']!=''){
            $user = User::where('name','like', "%" . $_GET['name'] . "%")->where('invoice_no','!=','')->paginate(40);
            }
        }
            else{
            //$user = User::where('invoice_no','!=','')->paginate(20);
            $user = User::where('invoice_no','!=','')->paginate(20);
            }
        return view('admin.member',compact('user','i'));
    }


    public function indexunpaid()
    {   $i=1;
        if(isset($_GET['name']) || isset($_GET['city']) || isset($_GET['state'])) {
            if($_GET['name']!='' && $_GET['state']!='' && $_GET['city']!='')
            {
                $user = User::where('name','like', "%" . $_GET['name'] . "%")
                ->where('state',$_GET['state'])->where('city',$_GET['city'])->where('invoice_no','=',Null)->paginate(40);
            }
            elseif($_GET['state']!='' && $_GET['city']!='')
            {
                $user = User::where('state',$_GET['state'])->where('city',$_GET['city'])->where('invoice_no','=',Null)->paginate(100);
            }
            elseif($_GET['state']!='')
            {
                $user = User::where('state',$_GET['state'])->where('invoice_no','=',Null)->paginate(100);
            }
            elseif($_GET['name']!=''){
            $user = User::where('name','like', "%" . $_GET['name'] . "%")->where('invoice_no','=',Null)->paginate(40);
            }
        }
            else{
            $user = User::where('invoice_no','=',Null)->paginate(20);
            }
        return view('admin.unpaidmember',compact('user','i'));
    }

   
public static function getdelegates($id)
{
    $del =Delegates::select('name')->where('user_id',$id)->get();
    return $del;
}


    public function promocodes()
    {   $i=1;
        $promo =Promocodes::select('promo_codes','status')->paginate(253);
        return view('admin.promolist',compact('promo','i'));
    }




    public function deletem($id){
        Businessctg::where('id',$id)->delete();
        Fdays::where('id',$id)->delete();
        Delegates::where('id',$id)->delete();
        User::where('id',$id)->delete();
         Session::flash('success','Queastion Deleted !!!');
         return redirect()->back();
     }




    public function accept($id)
    {
        Questions::where('id',$id)
        ->update(array('status' =>1,'updated_at' => Carbon::now()));
        Session::flash('success','Accepted');
        return redirect()->back();
    }


    

	public function activate($status,$id)
     {
         $activate = DB::table('poll_questions')
         ->where('id', $id)
         ->update(array('status'=> $status));

		  $activate1 = DB::table('poll_questions')
         ->where('id','!=', $id)
         ->update(array('status'=> '0'));

         if($activate) {
             Session::flash('success','Successfully Updated');
             return redirect()->back();
         }
     }



     public function session_list()
     {
        $i=1;
        $data = Event_session::Orderby('id','asc')->paginate(20);
        return view('admin.event_sessions',compact('data','i'));
     }
     public function activate_session($status,$id)
     {
         $activate =Event_session::where('id', $id)
         ->update(array('status'=> $status));

		  $activate1 = Event_session::where('id','!=', $id)
         ->update(array('status'=> '0'));

         if($activate) {
             Session::flash('success','Successfully Updated');
             return redirect()->back();
         }
     }



    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/admin');
    }


}
